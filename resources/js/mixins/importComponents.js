import Vue from 'vue'

export function importAll(r) {
    r.keys().map(key => {
        const value = key.match(/[-\w]+/)[0];
        return Vue.component(value, r(key).default);
    });
}
