import VueRouter from 'vue-router'
// Pages
import {_import_page} from './_import'
// Routes
const routes = [
    {
        path: '/',
        component: require('@/AppWrapper').default,
        meta: {
            auth: true,
            redirect: 'login',
            forbiddenRedirect: '403'
        },
        children: [
            // USER ROUTES
            {
                path: '/dashboard',
                name: 'dashboard',
                component: _import_page('user/Dashboard'),
            },
            // ADMIN ROUTES
            {
                path: '/admin',
                name: 'admin.dashboard',
                component: _import_page('admin/Dashboard'),
                meta: {
                    auth: {
                        roles: {
                            role_id: [1, 2]
                        },
                    }
                }
            },
            {
                path: '/permission',
                name: 'permission',
                component: _import_page('permission/index'),
                meta: {
                    auth: {
                        roles: {
                            role_id: [1, 2]
                        },
                    }
                }
            },
            {
                path: '/permission/create',
                name: 'permission.create',
                component: _import_page('permission/edit'),
                meta: {
                    auth: {
                        roles: {
                            permissions: ['write_permission']
                        },
                    }
                }
            },
        ]
    },
    {
        path: '/',
        component: require('@/AppBasic').default,
        children: [
            {
                path: '/login',
                name: 'login',
                component: _import_page('auth/Login'),
                meta: {
                    auth: false
                }
            },
            {
                path: '/404',
                name: 'error-404',
                component: _import_page('auth/404'),
                meta: {
                    auth: undefined
                }
            },
            {
                path: '/403',
                name: 'error-403',
                component: _import_page('auth/403'),
                meta: {
                    auth: undefined
                }
            },
        ]
    }
]
const router = new VueRouter({
    history: true,
    mode: 'history',
    routes,
})
export default router