import * as actions from './actions';

const store = {
    state: {
        permissionList: null,
        permissionCount: null
    },
    mutations: {
        GET_PERMISSION_LIST: (state, payload) => {
            state.permissionList = payload.data;
        },
        GET_PERMISSION_COUNT: (state, payload) => {
            state.permissionCount = payload.total;
        }
    },
    actions,
    getters: {
        permissionList: state => state.permissionList,
        permissionCount: state => state.permissionCount,
    }
};
export default store;
