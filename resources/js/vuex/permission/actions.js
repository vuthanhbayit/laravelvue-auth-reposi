import axios from 'axios'

export const getPermissionList = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        return axios({
            method: 'GET',
            url: '/permissions',
            params: opts
        }).then(response => {
            commit("GET_PERMISSION_LIST", response.data);
            commit("GET_PERMISSION_COUNT", response.data);
            return resolve(response);
        }).catch(err => {
            commit("GET_PERMISSION_LIST", null);
            commit("GET_PERMISSION_COUNT", null);
            return reject(err);
        })
    })
};
export const saveModule = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        return this.$http({
            data: {
                url: '/api/module/saveModule',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
export const deleteModule = ({commit}, opts) => {
    return new Promise((resolve, reject) => {
        return this.$http({
            data: {
                url: '/api/module/deleteModule',
                ...opts
            }
        }).then(response => {
            return resolve(response);
        }).catch(error => {
            return reject(error);
        })
    })
};
