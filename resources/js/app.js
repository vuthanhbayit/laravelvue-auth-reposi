// require('./bootstrap');
import 'bootstrap'
import 'es6-promise/auto'
import axios from 'axios'
import Vue from 'vue'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import App from './App.vue'
import auth from './auth'
import router from './router'
import store from './vuex'

// Set Vue globally
window.Vue = Vue

//import components
import {importAll} from "./mixins/importComponents";
importAll(require.context('./components', true, /\.vue$/));
require('./mixins/alert');

//config element ui
import {Select, Option, Loading} from 'element-ui';
import 'element-ui/lib/theme-chalk/select.css';
import 'element-ui/lib/theme-chalk/icon.css';
import 'element-ui/lib/theme-chalk/loading.css';
import langEl from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
locale.use(langEl);
Vue.component('el-select',Select);
Vue.component('el-option',Option);
Vue.use(Loading);
// Set Vue router
Vue.router = router
Vue.use(VueRouter)

// Set Vue authentication
Vue.use(VueAxios, axios)
// axios.defaults.baseURL = process.env.MIX_APP_URL + '/api';
axios.defaults.baseURL = '/api';
Vue.use(VueAuth, auth)
const app = new Vue({
    el: '#app',
    router,
    store,
    render: h=>h(App)
})